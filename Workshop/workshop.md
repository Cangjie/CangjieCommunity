# 【仓颉社区】Workshop
仓颉社区Workshop是仓颉社区发起的月度活动，由仓颉生态组主办、编译星火与OpenHarmony编程语言TSC联合出品的社区交流分享活动，社区灵感碰撞与编程技术交流，欢迎各位参与。
## 往期回顾
|活动|活动时间|分享议题|演示文稿|视频回顾|
|---|---|---|---|---|
|第30期|20250222| 1️⃣《Java2CJ代码转换工具介绍》 - 郭老师@华为<br>2️⃣《鸿蒙应用三方库Lottie的设计与实现》 - 李老师@三方库工程师<br>3️⃣《开发者空间介绍及仓颉编程语言开发》 - 郭老师@华为云DTSE布道师|[PDF文稿](https://gitcode.com/Cangjie/CangjieCommunity/tree/main/Workshop/%E7%AC%AC30%E6%9C%9FWorkshop)|[视频链接](https://www.bilibili.com/video/BV1XgPhe3ESX/)|
|第29期|20250124| 1️⃣《仓颉兴趣组趣味项目分享》 - 张引@东北大学<br>2️⃣《泛型特性的变化 -- 前瞻》 - 轩加振@华为<br>3️⃣《仓颉鸿蒙项目实战：仿豆包APP的UI实现》 - 徐迪@杭州电子科技大学|[PDF文稿](https://gitcode.com/Cangjie/CangjieCommunity/tree/main/Workshop/%E7%AC%AC29%E6%9C%9FWorkshop)|[视频链接](https://www.bilibili.com/video/BV17CfnY3ETs/)|
|第28期|20241228| 1️⃣《仓颉编程语言2024生态建设总结》 - 王学智@华为<br>2️⃣《毕方AI代码辅助工具FireCoder介绍》 - FireCoder小方@华为<br>3️⃣《GISTools仓颉库的设计和开发》 - 董昱 |[PDF文稿](https://gitcode.com/Cangjie/CangjieCommunity/tree/main/Workshop/%E7%AC%AC28%E6%9C%9FWorkshop)|[视频链接](https://www.bilibili.com/video/BV1jJCaYaEP9/)|
|第27期|20241130| 1️⃣《基于仓颉语言的三方库CJQT介绍》- 周丽波@上海双洪<br>2️⃣《仓颉并发机制的设计与实现》- 陈冬杰@华为<br>3️⃣《仓颉版Kafka客户端示例展示与开发心得》- 蔡学文@东方通|[PDF文稿](https://gitcode.com/Cangjie/CangjieCommunity/tree/main/Workshop/%E7%AC%AC26%E6%9C%9FWorkshop)|[视频链接](https://www.bilibili.com/video/BV1x16PYFEPy/)|
|第26期|20241031|1️⃣《仓颉编程语言官网介绍》- 朴冠羽@华为<br>2️⃣《神农为服务开发套件介绍》- 昝望@华为<br>3️⃣《仓颉语言开源微服务框架microservice开发实践分享》- 赵刚@上汽享道出行<br>4️⃣《仓颉在HarmonyOS NEXT AI大模型端侧部署的实践与创新挑战》- 邓顺子|[PDF文稿](https://gitcode.com/Cangjie/CangjieCommunity/tree/main/Workshop/%E7%AC%AC27%E6%9C%9FWorkshop)|[视频链接](https://www.bilibili.com/video/BV1FFS9Y7E7N)|
|第25期| 20240927 |1️⃣科蓝金融交易回溯系统仓颉版示范应用成功案例分享<br>2️⃣智能·协同·信创-仓颉+泛微 构建智慧公文平台<br>3️⃣仓颉开发鸿蒙原生应用的示例展示与开发心得 |[PDF文稿](https://gitcode.com/Cangjie/CangjieCommunity/tree/main/Workshop/%E7%AC%AC25%E6%9C%9FWorkshop) | [视频链接]()|
|第24期| 20240831 |1️⃣类型推断技术及仓颉语言实践<br> 2️⃣OBS仓颉版客户端SDK实现<br>3️⃣宏编程实践之派生宏<br>4️⃣LeetCode三方库Editor |[PDF文稿](https://gitcode.com/Cangjie/CangjieCommunity/tree/main/Workshop/%E7%AC%AC24%E6%9C%9FWorkshop/slide) | [视频链接](https://www.bilibili.com/video/BV1FXHze4EN2/ )|
| 第23期 | 20240727 | 1️⃣编程语言High-Level IR设计--以仓颉为例<br>2️⃣仓颉Redis客户端的实现<br>3️⃣仓颉版S3客户端介绍<br>4️⃣基于仓颉的Markdown渲染库实现| [PDF文稿](https://gitcode.com/Cangjie/CangjieCommunity/tree/main/Workshop/%E7%AC%AC23%E6%9C%9FWorkshop/slide) | [视频链接](https://www.bilibili.com/video/BV1YW42197bP/)|